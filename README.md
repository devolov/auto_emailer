# Cat Facts
#### Inspired by [this meme](https://i.imgur.com/uWqpr.jpeg) from the late aughts.

Code was made by [David Volovskiy](https://www.linkedin.com/in/davidvolovskiy/) as a hobby project to learn:
- CI with Gitlab
- Get comfortable with IMAP and STMP
- Continuous brushing up on Python
- How to annoy my friends automatically
#
| File | Purpose |
| ------ | ------ |
| **Auto_Emailer.py** | The Python code that sends an email from the *Sender* to the *Targets* in *Emails.yml* with cat trivia found in *Facts.yml*. It also parses through the sender's inbox to see if anyone else subscribed.|
| **Emails_template.yml** | A template of what the *Emails.yml* file should look like. There's obvious security risks with hosting the email's App Password and mailing list on Gitlab, so only a template is here. |
| **Facts.yml** | The list of trivia to send to a user. The trivia is sent in order and the *"subscription"* to is terminated once all the facts are sent. Cat facts were found [here](https://cvillecatcare.com/veterinary-topics/101-amazing-cat-facts-fun-trivia-about-your-feline-friend/), plant facts were found [here](https://crazycrittersinc.com/over-50-amazing-plant-facts/), and bee facts were found [here](https://www.beepods.com/101-fun-bee-facts-about-bees-and-beekeeping/).|
| **.gitlab-ci.yml** | The Gitlab Continuous Integration file. It checks for dependency risks using [`safety check`](https://pyup.io/safety/) and runs the test function of *Auto_Emailer.py*. |
| **requirements.txt** | Python dependencies that need to be installed using `python -m pip install -r requirements.txt` |
| **catfacts.service** | The configuration file for [systemd](https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6) so this program can exist on a stand-alone Raspberry Pi and reset if the program dies from something like a power outage. |
| **cat_facts_logo.svg** | Vector graphic of Cat Facts logo. |
| **.gitignore** | Git ignore field to avoid GIT from tracking the original *Email.yml* file and virtual environment |
|**deploy_autoemailer.sh**| Bash script that updates and deploys the most current commit in the repo |
| **README.md** | The document you're currently reading |

## How to Set it Up to Run With a Gmail Account
1. [Create a new Gmail account.](https://support.google.com/mail/answer/56256?hl=en)
2. [Create an App Password.](https://support.google.com/accounts/answer/185833) 
   * [Note that allowing Less-Secure Apps to access Gmail is no longer an option.](https://support.google.com/accounts/answer/6010255?rfn=1646361360139&anexp=nret-fa&pli=1)
3. Create a label called *Acted* and *Not_Acted*.
4. Clone this repo into it using HTTPS with `git clone https://gitlab.com/devolov/auto_emailer.git`
5. Make a copy of the *Emails_template.yml* and name is *Emails.yml*
6. Copy the username and App Password created into the *Sender* fields.
7. For the *Target* object, it can be modified manually with valid emails or all of its entries can be deleted as they'll auto-populate as people subscribe.
8. For the *Next_Send_Data*, it can be set for the next time to set a message. 
   *  *hr_next: 13*, *min_next: 31* means the message will be sent at 1:31 PM.
9. Create a virtual environment for testing using `python -m venv venv`
10. Start the virtual environment by running `venv\Source\activate` in Windows or `source venv/bin/activate` in Linux.
11. Install the dependencies with `python -m pip install -r requirements.txt`
12. run the python using `python Auto_Emailer.py`
13. The print statements should let you know quickly whether it's functioning.

Stream of commands for easier copying:
```
git clone https://gitlab.com/devolov/auto_emailer.git
python -m venv venv
venv\Source\activate
python -m pip install -r requirements.txt
python Auto_Emailer.py
```

## How to Run It In A Raspberry Pi:
1. Make sure you have the code running in the *How to Set it Up to Run With a Gmail Account* section first.
2. Install [Raspbian](https://www.raspberrypi.com/software/) on the Raspberry Pi.
   * In the [Raspbian imager](https://www.tomshardware.com/news/raspberry-pi-imager-now-comes-with-advanced-options), set up the wifi credentials and SSH settings. It's way easier to do it before the image is flashed.
   * While out of the scope of this tutorial, I will always recommend [turning off the ability to SSH into the Pi with a password](https://hackaday.io/project/5771-ssh-keys-for-raspberry-pi/log/17774-disable-ssh-by-password) and [setting it up to use an SSH key.](https://www.raspberrypi-spy.co.uk/2019/02/setting-up-ssh-keys-on-the-raspberry-pi/)
3. Boot the Pi up and either SSH into it or operate it directly.
4. Create a new user named *catfacts* using `sudo useradd catfacts`
5. Either give the new user a password with `sudo passwd catfacts` or remove the need for it to need a password with `passwd -d catfacts`
6. Create a new group for user *catfacts* called *catfacts* using `sudo groupadd catfacts`
7. Add user *catfacts* to the group *catfacts* with `sudo usermod -a -G catfacts catfacts`
8. Create a folder in *~/* called *GIT* with `mkdir ~/GIT` and navigate to it with ` cd ~/GIT/`
9. Clone this repo into it using HTTPS with `git clone https://gitlab.com/devolov/auto_emailer.git`
10. Create and modify *Emails.yml* from `Emails_template.yml` as mentioned earlier.
11. Create a folder in *opt* called *Cat_Facts* with `sudo mkdir /opt/Cat_Facts`
12. Copy the files to the new directory using `sudo cp -R ~/GIT/auto_emailer/* /opt/Cat_Facts`
13. Change the ownership of the new files in */opt/Cat_Facts/* to *catfacts* with `sudo chown -R catfacts:catfacts /opt/Cat_Facts/`
14. Verify the ownership has been changed with `ls -la /opt/Cat_Facts/`
15. Install *pip* with `sudo apt-get install python3-pip`
16. Install the dependencies with `pip install -r requirements.txt`
17. Move the *catfacts.service* to the systemd config folder with `sudo cp ~/GIT/auto_emailer/catfacts.service /etc/systemd/system/`, making sure the owner in that file matches the one whose ownership was set in the */opt/Cat_Facts/* folder.
18. Restart the systemd services with `sudo systemctl daemon-reload`
19. Start Cat Facts by running `sudo systemctl start catfacts.service`
20. Make the program automatically start on boot-up by running `sudo systemctl enable catfacts.service`
21. To see the last print statements made by the service, run `sudo journalctl -ru catfacts.service`

Stream of commands for easier copying:
```
sudo useradd catfacts
sudo passwd catfacts
passwd -d catfacts
sudo groupadd catfacts
sudo usermod -a -G catfacts catfacts
mkdir ~/GIT
git clone https://gitlab.com/devolov/auto_emailer.git
sudo mkdir /opt/Cat_Facts
sudo cp -R ~/GIT/auto_emailer/* /opt/Cat_Facts
sudo chown -R catfacts:catfacts /opt/Cat_Facts/
ls -la /opt/Cat_Facts/
sudo apt-get install python3-pip
pip install -r requirements.txt
sudo cp ~/GIT/auto_emailer/catfacts.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start catfacts.service
sudo systemctl enable catfacts.service
sudo journalctl -ru catfacts.service
```

## How to Update the Raspberry Pi Code After Deploying
**Note:** This assumes the newer version of the code can still use the previous yml files and is still all-contained in *Auto_Emailer.py*. This should be the case if the major version number is the same.
1. SSH or directly operate the Pi
2. Navigate to the GIT repo with `cd ~/GIT/auto_emailer/`
3. Make the current branch is *master* with `git branch`
4. Pull the most recent code with `git pull`
5. Remove the old version of *Auto_Emailer.py* from the */opt/* folder with `sudo rm /opt/Cat_Facts/Auto_Emailer.py`
6. Copy the newer version of *Auto_Emailer.py* to that folder with `sudo cp Auto_Emailer.py /opt/Cat_Facts/.`
7. Change the file's ownership by running `sudo chown catfacts:catfacts /opt/Cat_Facts/Auto_Emailer.py`
8. Make sure it copied over and the ownership changed by viewing the directory with `ls -la /opt/Cat_Facts/`
9. Restart the systemd service of *catfacts* with `sudo systemctl restart catfacts`
10. Check the outputs of the code to make sure it's running with `sudo journalctl -ru catfacts.service`

Or
1. SSH or directly operate the Pi
2. Navigate to the GIT repo with `cd ~/GIT/auto_emailer/`
3. Make the file an executable for the current user by running `chmod u+x ./deploy_autoemailer.sh`
4. Run *deploy_autoemailer.sh* by running `./deploy_autoemailer.sh`
5. Review the results by checking the output of the *git branch* and *jounralctl* commands to make sure the correct branch was deployed and the output of *journalctl* looks correct.

Stream of commands for easier copying:
```
cd ~/GIT/auto_emailer/
git branch
git pull
sudo rm /opt/Cat_Facts/Auto_Emailer.py
sudo cp Auto_Emailer.py /opt/Cat_Facts/.
sudo chown catfacts:catfacts /opt/Cat_Facts/Auto_Emailer.py
ls -la /opt/Cat_Facts/
sudo systemctl restart catfacts
sudo journalctl -ru catfacts.service
chmod u+x ./deploy_autoemailer.sh
./deploy_autoemailer.sh
```

## How to Run Test Function
1. Make sure you have the code running in the *How to Set it Up to Run With a Gmail Account* section first.
2. Run the command: `python Auto_Emailer.py --test`
3. The function should return the last few elements in *Cat_Facts.yml* and a sample message that would be sent. This proves that the yml reading, random time generation, and message creation functions work.
   * The test function is limited due to the email and password not being saved in the GIT repo. 
   * The goal of the test function is to run as a CI in Gitlab.

Stream of commands for easier copying:
```
python Auto_Emailer.py --test
```

## How Run Gitlab-Runner on Your Windows Machine
**Note:** At the time of writing (12/2021), gitlab-runner with Docker on Windows is [kind of broken](https://stackoverflow.com/questions/53472481/gitlab-runner-docker-windows-invalid-volume-specification), so using WSL is a good work-around.
1. [Install Ubuntu on your Windows machine as a WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
2. [Install docker on desktop](https://docs.docker.com/desktop/windows/install/)
3. In the Docker Desktop, go to *Settings (the gear icon)* > *Resources* > *WSL Integration*. 
Make sure *Enable integration with my default WSL distro* is checked and enable *Ubuntu* in the the *Enable integration with additional distros* option.
4. Open command prompt, navigate to folder where this GIT repo exists, and run `wsl`
5. Make sure the current working directory is the GIT repo by running `ls` to list the files in the directory.
5. Once in the WSL shell, make sure Docker is integrated by running `docker`
6. Install gitlab-runner by running:
    * `curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash`
    * `sudo apt-get install gitlab-runner`
7. Install pip with `apt install python3-pip`
8. Install virtual environments with `python3 -m pip install virtualenv`
9. Run a task found in the *.gitlab-ci.yml* file by running `gitlab-runner exec docker <task>`
   * **Example:** `gitlab-runner exec docker check_dep`

Stream of commands for easier copying:
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
apt install python3-pip
python3 -m pip install virtualenv
gitlab-runner exec docker check_dep
```

## How People are to Subscribe and Unsubscribe
* On phone, a user should be able to subscribe by texting *subscribe* or *SUBSCRIBE* to the Gmail address.
* To unsubscribe, they should be able to by texting: *unsubscribe* or *UNSUBSCRIBE*.
* On email, sending the subject *unsubscribe* or *UNSUBSCRIBE* to the Gmail address should allow to subscribe.
* To unsubscribe, they should be able to by emailing with the subject: *unsubscribe* or *UNSUBSCRIBE*.
* It's more hit-and-miss, but sending *subscribe* or *SUBSCRIBE* in the body often works as well, but due to how each email provider encodes their emails in MIME a little different, there are many edge-case.
  * **For example:** Sending an email from Outlook with an automatic signature causes the signature to appear with the message. Reading for any plain-text that shows *subscribe* or *SUBSCRIBE*  would solve this, but now any email that includes a line that says *subscribe* will be added to the list, which could be from a spam email. A false-negative is preferable to a false-positive in the case of this program.
* Sending  *subscribe*, *SUBSCRIBE*, *unsubscribe*, or *UNSUBSCRIBE* with any other characters in the email will not trigger an event. Text messages can trigger if they have *\n* or *\r*, but nothing else, including spaces.
* While it requires manual editing of the *Facts.yml* file, the user can go from having *Cat Facts* to *Plant Facts* by changing the *Trivia_type* from *Cat* to *Plant*

Use Python 3.7 or higher. If not, you'll need to make the *TRIVIA_ARRAY* an OrderedDict using *collections.OrderedDict* in *Auto_Emailer.py*.

Tags and version numbering follows [**Semantic Versioning 2.0**](https://semver.org/spec/v2.0.0.html).

