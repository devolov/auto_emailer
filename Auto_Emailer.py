import datetime
from sys import argv
from pytz import timezone as tz
from random import randint, seed
from smtplib import SMTP_SSL, SMTPAuthenticationError
from ssl import create_default_context
from time import sleep
from yaml import safe_load, dump
from imaplib import IMAP4_SSL
from email import message_from_bytes
from copy import deepcopy
import logging
import re

# Allow Python to send emails through Gmail, Allow less secure apps needs to be set to ON.
# https://myaccount.google.com/lesssecureapps
# Create a label in you Gmail called the DEST_BOX_ACTED and DEST_BOX_NOT_ACTED global consts.
# Note: don't have spaces in the label names. It might work for you, but broke the code when I tried it

"""
Global Constants
"""
DISARM = False  # If true, then functions that send out emails or move emails are disabled. For debugging
TRIG_CODE_AT_START = False  # If true, the program will start sending texts right when it launches
TRIVIA_ARRAY = []
SUB_THRU_ALL_CATEGORIES = True  # If True, then once the current list of trivia is done, it'll go to the next one
CATEGORY_DEFAULT = None  # It will get written with the first key in TRIVIA_ARRAY once read
# Inboxes
SRC_BOX = "inbox"
DEST_BOX_ACTED = "Acted"
DEST_BOX_NOT_ACTED = "Not_Acted"
SUB_TXT = "SUBSCRIBE".upper()  # Keep uppercase; this is to avoid case-sensitive issues
UNSUB_TXT = "UNSUBSCRIBE".upper()  # Keep uppercase; this is to avoid case-sensitive issues
MSG_PRINT_LEN = 20  # The max length of a message to print to avoid a long email cluttering the logs
# Time
TIMEZONE = 'EST'
HOUR_EARLIEST = 9  # Do not send texts earlier than this hour
HOUR_LATEST = 17  # Do not send texts later than this hour
HOUR_DEFAULT = 9  # If the yaml has invalid hour data, use this
MINUTE_DEFAULT = 30  # If the yaml has invalid minute data, use this
BOUNDS = 15  # The next text will not be less than bounds min from the prev nor more than 60-bounds away
SEED_RAND = 50  # The seed for choosing random time
SEC_EMAIL_CHECK = 30  # Keep below 60. The amount of sec between checks of inbox and if we're at the trigger time.

"""
Set up logger for easier debugging
Ex output: 2022/01/01 11:09:38 (INFO): Output data
"""
logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)s: %(message)s'
)


class Sender:
    """
    Sender class for easy movement of the sender's name, email and password
    """

    def __init__(self, email_addr, password, name):
        self.email = email_addr
        self.password = password
        self.name = name


class TimeTrig:
    """
    TimeTrig class holds the last time we wanted to trigger as a datetime, along with the next time to trig as a
    datetime and the next time to trig as a string (Ex: 1:42 PM)
    """

    def __init__(self, hr_to_trig, min_to_trig, date_to_trig):
        self.last_wanted_trig = datetime.datetime(2000, 1, 1)
        # Set to arbitrary time before now since trig didn't occur yet
        self.next_trig = datetime.datetime.combine(date=date_to_trig,
                                                   time=datetime.time(hour=hr_to_trig, minute=min_to_trig, second=0))

    @property  # Automatically run this whenever getting next_trig_str, so it's always updated
    def next_trig_str(self):
        return self.next_trig.strftime("%I:%M %p").lstrip("0")

    def create_next_trig(self, hr_to_trig, min_to_trig, curr_time=None):
        if curr_time is None:
            curr_time = tz(TIMEZONE).localize(datetime.datetime.now())
        days_delay = 1
        if self.next_trig is not None:
            self.last_wanted_trig = self.next_trig
        self.next_trig = (curr_time + datetime.timedelta(days=days_delay)).replace(hour=hr_to_trig,
                                                                                   minute=min_to_trig, second=0)


def create_message(rcvr_name, trivia_index, sender_info: Sender, time_data: TimeTrig, target_email,
                   sub_unsub="Unchanged", triv_type=None):
    """
    Creates the message to be sent
    :param rcvr_name: Name of the person getting the message. Comes from the yaml in Target
    :param trivia_index: Fact_Next of the person getting the message. Comes form the yaml n Target
    :param sender_info: the sender object of the person sending it, so it's not an anonymous Fact
    :param time_data: The TimeTrig object containing the last and next time the message was sent
    :param target_email: The email that is being sent to
    :param sub_unsub: "Unchanged" = The target has not subbed nor unsubbed
                "Subscribe" = The target has subscribed just before this call
                "Unsubscribe" = The target has unsubscribed just before this call
    :param triv_type: The key of the trivia category in the yaml file being sent
    :return: the text message the receiver will get; If the User is out of messages, returns None
    """
    global TIMEZONE, CATEGORY_DEFAULT, SUB_THRU_ALL_CATEGORIES
    if triv_type not in TRIVIA_ARRAY.keys():
        logging.warning(f"Requested trivia is not in yml file. Found {triv_type}. Defaulting to Cat")
        triv_type = CATEGORY_DEFAULT
    message = msg_init = "\n"  # Messages will need to begin with \n
    time_curr = tz(TIMEZONE).localize(datetime.datetime.now())
    # subject is used if the message is sent to an email, and not a phone number
    curr_date_str = time_curr.strftime("%b-%d-%Y")  # Ex: Jan-01-2022
    curr_date_wkdy_str = time_curr.strftime("%a, %b-%d-%Y")  # Ex: Sat, Jan-01-2022
    subject = f"{triv_type} Fact of the Day | {curr_date_str}"
    if sub_unsub == "Unchanged":
        if time_curr.date() != time_data.last_wanted_trig.date():
            message += f"We apologize for the interruption in {triv_type} Facts.\n"
        if trivia_index < len(TRIVIA_ARRAY[triv_type]):  # Messages are left
            message = holiday_text(message, time_curr)
            message += f"{triv_type} Fact of the Day"
            message += " " if parse_email_for_phone(target_email) is None else "\n"
            # if phone #, use \n and not space; this is due to smaller screen size in text msgs causing
            # the date string to not come out well when sharing a line with the fact type
            message += f"({curr_date_wkdy_str}):\n\n{TRIVIA_ARRAY[triv_type][trivia_index - 1]} " \
                       f"\n\n\nThanks for Appreciating " \
                       f"this {triv_type} Fact! Your next {triv_type} Fact will come tomorrow at " \
                       f"{time_data.next_trig_str}"
            if parse_email_for_phone(target_email) is not None:
                # if it's a phone number, the time to send is less reliable than email
                message += " or later"
            message += f".\nTo unsubscribe, send the message: {UNSUB_TXT}"
        elif trivia_index == len(TRIVIA_ARRAY[triv_type]):  # Last Message in that Fact
            triv_type_next = get_next_key_in_dict(triv_type) if SUB_THRU_ALL_CATEGORIES else None
            if triv_type_next is None:  # Completely Out of Facts
                logging.info(f"{target_email} has no other trivia to look at after {triv_type}.")
                message += f"{triv_type} Fact of the Day:\n\n{TRIVIA_ARRAY[triv_type][trivia_index - 1]} \n\n\n" \
                           f"Thanks for Appreciating this {triv_type} Fact! This concludes your subscription."
            else:
                logging.info(f"{target_email} is now subbed to {triv_type_next}.")
                triv_type = triv_type_next
                message, _ = create_message(rcvr_name, 1, sender_info, time_data, target_email,
                                            sub_unsub="Subscribe", triv_type=triv_type)
                return message, triv_type
        else:
            logging.info(f"{target_email} is out of facts.")
            # Out of facts. sub is over
            return None, triv_type
    elif sub_unsub == "Subscribe":
        # If next message is set at a time that no longer happens today
        if time_data.next_trig.date() == time_curr.date():
            today_or_tmrw_txt = "today at "
        else:
            today_or_tmrw_txt = "tomorrow at "
        today_or_tmrw_txt += f"{time_data.next_trig_str}"
        if parse_email_for_phone(target_email) is not None:
            # if it's a phone number, the time to send is less reliable than email
            today_or_tmrw_txt += " or later"
        if len(rcvr_name) > 0:
            rcvr_name = " " + rcvr_name.upper()  # Uppercases the name and adds a space to the beginning of it
            # for when combining the string
        if trivia_index > 1:  # If they've subscribed before
            message += f"WELCOME BACK{rcvr_name}!\n\nThe Trivia will continue where you left off. Your next " \
                       f"{triv_type} Fact is scheduled for {today_or_tmrw_txt}" \
                       f".\nTo unsubscribe, send the message: {UNSUB_TXT}"
        else:
            message += f"HI{rcvr_name}! WELCOME TO {triv_type.upper()} FACTS!\n\nY" \
                       f"our first {triv_type} Fact is scheduled for {today_or_tmrw_txt}." \
                       f"\nTo unsubscribe, send the message: {UNSUB_TXT}"
    elif sub_unsub == "Unsubscribe":
        message += f"Sorry to see you leave {triv_type} Facts.\nTo resubscribe, simply send the message: {SUB_TXT}"
    else:
        logging.warning(f"Incorrect sub_unsub entered, Seeing: {sub_unsub}")
    if message != msg_init:
        message += f"\nCourtesy of {sender_info.name}."
        if parse_email_for_phone(target_email) is None:  # if not phone number
            message = f"""From: {target_email}\nTo: {sender_info.email}\nSubject: {subject}\n\n{message}"""
    else:
        message = None
    logging.debug(f"Message Created: {message}")
    return message, triv_type  # returning triv_type because it can change once you're out of one type of fact


def holiday_text(message, time_curr):
    """
    Adds the holiday to the beginning of a message
    :param message: The message string to add to
    :param time_curr: The current time in datetime format. Used to get the date
    :return: The message string with a "happy <holiday>" added to it
    """
    if time_curr.month == 1 and time_curr.day == 1:
        message += "Happy New Year!\n"
    elif time_curr.month == 1 and time_curr.weekday() == 0 and 15 <= time_curr.day <= 21:
        # if it's Jan, a Monday, and in the third week of the month
        message += "Happy MLK Day!\n"
    elif time_curr.month == 2 and time_curr.day == 14:
        message += "Happy Valentine's Day!\n"
    elif time_curr.month == 3 and time_curr.day == 17:
        message += "Happy St. Patrick's Day!\n"
    elif time_curr.month == 3 and time_curr.day == 26:
        message += "Happy Jaime Ettinger Day!\nhttps://okjaime.net/isitmybirthday.html\n"
    elif time_curr.month == 5 and time_curr.day == 5:
        message += "Happy Cinco de Mayo!\n"
    elif time_curr.month == 5 and time_curr.weekday() == 0 and 25 <= time_curr.day:
        # if it's May, a Monday, and in the last week of the month
        message += "Happy Memorial Day!\n"
    elif time_curr.month == 6 and time_curr.day == 19:
        message += "Happy Juneteenth!\n"
    elif time_curr.month == 7 and time_curr.day == 4:
        message += "Happy Fourth of July!\n"
    elif time_curr.month == 9 and time_curr.weekday() == 0 and time_curr.day <= 7:
        # If it's Sept, a Monday, and the first week in the month
        message += "Happy Labor Day!\n"
    elif time_curr.month == 11 and time_curr.day == 11:
        message += "Happy Veterans Day!\n"
    elif time_curr.month == 11 and time_curr.weekday() == 3 and 22 <= time_curr.day <= 28:
        # if it's Nov, a Thursday, and in the fourth week of the month
        message += "Happy Thanksgiving!\n"
    elif time_curr.month == 12 and time_curr.day == 25:
        message += "Merry Christmas!\n"
    return message


def send_emails(emails_target, time_data, sender_info: Sender, cred_test=False, sub_unsub="Unchanged"):
    """
    Sends out the emails to the subscribers in the mailing list or a new subscriber/unsubscriber
    :param emails_target: array of names, emails, sub status, and trivia counter of people to text
    :param time_data: The TimeTrig object containing the last and next time the message was sent
    :param sender_info: The name, password, and email of the sender in the Sender object
    :param cred_test: If True, this function will only attempt to log into the sender's email and
    return False if it cannot
    :param sub_unsub: Whether the user is subbing, unsubbing, or no change
    :return: if cred_test = True: True if email and password are valid credential; False o/w
             if cred_test = False: emails_target updated (if a message was sent, the emails_target's trivia counter
             will go up for example)
    """
    global DISARM, CATEGORY_DEFAULT, SUB_THRU_ALL_CATEGORIES
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    context = create_default_context()
    with SMTP_SSL(smtp_server, port, context=context) as server:
        try:
            server.login(sender_info.email, sender_info.password)
            if not cred_test:
                if emails_target is None:  # If the string of emails is empty
                    return emails_target
                for target in emails_target:
                    receiver_name = target['Name']
                    receiver_email = target['Email']
                    receiver_trivia = target['Fact_Next']
                    receiver_subbed = target['Subscribed']
                    receiver_fact_type = target['Trivia_type']
                    recvr_title = receiver_email if receiver_name == '' else receiver_name
                    txt_or_eml_str = "email" if parse_email_for_phone(receiver_email) is None else "text"
                    if sub_unsub != "Unchanged" or receiver_subbed:
                        message, fact_type_new = create_message(receiver_name, receiver_trivia, sender_info, time_data,
                                                                receiver_email, sub_unsub, triv_type=receiver_fact_type)
                        if message is None:  # None means the user is out of messages and is unsubbed
                            target['Subscribed'] = False
                            target['Fact_Next'] = 1  # Resets to the first fact in case they'd like to re-sub
                            target['Trivia_type'] = CATEGORY_DEFAULT if SUB_THRU_ALL_CATEGORIES else receiver_fact_type
                        else:
                            if DISARM:
                                logging.info(f"Disarmed; would be sending {txt_or_eml_str} to {recvr_title}")
                            else:
                                logging.info(f"Sending {txt_or_eml_str} to {recvr_title}")
                                server.sendmail(sender_info.email, receiver_email, message.encode("utf-8"))
                                # Above is line that sends emails
                            if receiver_fact_type != fact_type_new:  # Happens if the user is now subbed to the
                                # next category due to SUB_THRU_ALL_CATEGORIES being on
                                target['Trivia_type'] = fact_type_new
                                target['Fact_Next'] = 1
                            elif sub_unsub == "Unchanged":
                                target['Fact_Next'] += 1
                    elif not receiver_subbed:
                        logging.info(f"Not {txt_or_eml_str}ing unsubbed user {recvr_title}")
                server.close()
                return emails_target
            else:
                server.close()
                return True
        except SMTPAuthenticationError as e:
            logging.error(f"{e} exception when attempting to send emails")
            return False


def modify_next_time_sent(next_send):
    """
    Changes the time to send a message if the next day is significant
    :param next_send: The datetime object for when to send the next text
    :return: next_send - The datetime object with its time to send modified based on the date
    """
    hour = minute = None
    if next_send.date().month == 4 and next_send.date().day == 20:
        # For no significant reason: if it's 4/20, send a message at 4:20 PM
        hour = 16
        minute = 20
    elif next_send.date().month == 11 and next_send.date().day == 11:
        # If it's 11/11, send a message at 11:11 AM
        hour = 11
        minute = 11
    if hour is not None:
        next_send = next_send.replace(hour=hour, minute=minute, second=0)
        logging.info(f"Due to the next day's date, next time to send it {hour}:{minute}")
    return next_send


def random_hour(hr_last):
    """
    :return: a random hour within HOUR_EARLIEST and HOUR_LATEST and is not hr_last
    """
    global HOUR_EARLIEST, HOUR_LATEST
    hr_next = randint(HOUR_EARLIEST, HOUR_LATEST)  # Get the next minute the code will be sent
    if hr_next == hr_last:  # So next hr won't occur on the same hour  as the last
        return random_hour(hr_last)
    else:
        return hr_next


def random_min(min_last):
    """
    Chooses a random minute for the next hour;
    Next minute will be between BOUND and (60-BOUND) minutes away from min_last and will not be the same minute
    :param min_last: The last minute triggered
    :return: min_next - The next minute to trigger
    """
    global BOUNDS
    min_min = 60 - min_last + BOUNDS if (min_last + BOUNDS) > 60 else 0
    min_max = 60 + min_last - BOUNDS if min_last < BOUNDS else 59
    min_next = randint(min_min, min_max)  # Get the next minute the code will be sent
    if min_next == min_last:  # So next min won't occur on the same minute as the last
        return random_min(min_last)
    else:
        return min_next


def get_yml_time_data(time_yml_data):
    """
    Reads the items in the yaml dictionary for time and returns the data in it if it's valid.
    Otherwise, it returns the default time.
    :param time_yml_data: ['Next_Send_Data'] in the yml
    :returns: The hour and minute to next trigger
    """
    global HOUR_DEFAULT, MINUTE_DEFAULT, HOUR_EARLIEST, HOUR_LATEST
    # First hour that the code triggers at if Trig_At_Code_Start isn't called
    try:
        hr_trig = time_yml_data['hr_next']
    except KeyError:
        hr_trig = None
    if not isinstance(hr_trig, int) or hr_trig not in range(HOUR_EARLIEST, HOUR_LATEST+1, 1) or hr_trig is None:
        hr_trig = HOUR_DEFAULT
        logging.warning(f"Issue reading hour in file; defaulting to: {str(hr_trig)}")
    # First minute that the code triggers at if Trig_At_Code_Start isn't called
    try:
        min_trig = time_yml_data['min_next']
    except KeyError:
        min_trig = None
    if not isinstance(min_trig, int) or min_trig not in range(0, 60, 1) or min_trig is None:
        min_trig = MINUTE_DEFAULT
        logging.warning(f"Issue reading minute in file; defaulting to: {str(min_trig)}")
    try:
        date_trig = time_yml_data['date_next']
    except KeyError:
        date_trig = None
    curr_time = tz(TIMEZONE).localize(datetime.datetime.now())  # Get the current time
    if not isinstance(date_trig, datetime.date) or date_trig is None:  # Is None redundant here, but for readability
        date_trig = curr_time.date()
        logging.warning(f"Issue reading date in file; defaulting today: {str(date_trig)}")
    return hr_trig, min_trig, date_trig


def get_next_key_in_dict(prev_key, dict_to_check=None):
    """
    Finds the next key in a dict
    :param prev_key: The key that is the one before the one we want in the dict
    :param dict_to_check: The dictionary to check the keys of
    :return: The next key in a dict, None if no more keys
    """
    if dict_to_check is None:
        dict_to_check = TRIVIA_ARRAY
    dict_keys = list(dict_to_check.keys())
    # As of Python 3.7, dicts are ordered, so the keys should be in order as well.
    for i, key in enumerate(dict_keys):
        if key == prev_key:
            try:
                return dict_keys[i + 1]
            except IndexError:
                return None


def connect(email_addr, password):
    """
    Connects via IMAP to email to be able to comb through emails
    :param email_addr: Email address being looked at
    :param password: password of email address being looked at
    :return: imap - an object that represents the emails in the inbox
    """
    imap = IMAP4_SSL("imap.gmail.com")
    imap.login(email_addr, password)
    return imap


def disconnect(imap):
    """
    Disconnects from email server with the imap object
    :param imap: an object that represents the emails in the inbox
    :return: Nothing
    """
    imap.logout()


def parse_email_for_phone(email_addr):
    """
    Checks if an email is a phone number by seeing if the email address before the @ is either all numbers
    or all numbers and starts with a '+'
    :param email_addr: Email address to check
    :returns: If not a number = None; If number = The digits of the number
    """
    if email_addr.startswith("+"):
        email_addr = email_addr[1:]  # Removes the plus sign at the beginning of the string if it exists
    match = re.match(r'(?P<phone>\d*)@', email_addr)
    if match is None:
        return None
    else:
        return match.group('phone')


def parse_uid(data):
    """
    Uses regex to take the Unique ID that is given from IMAP and spits out the single number as a string
    :param data: The UID formatted in how IMAP's fetch command outputs it. Ex: 1 (UID 99)
    :return: The UID as a lone string Ex of above: 99
    """
    match = re.match(r'\d+ \(UID (?P<uid>\d+)\)', data)
    if match is None:
        return None
    else:
        return match.group('uid')


def check_for_new_mail(email_addr, password):
    """
    See what mail is in the inbox at first glance along with returning the mailbox object as imap
    :param email_addr: Email address being looked at
    :param password: password of email address being looked at
    :return: imap - object of the email's inbox; email_ids - the identifiers of the emails in the label
    """
    global SRC_BOX
    imap = connect(email_addr, password)
    imap.select(mailbox=SRC_BOX, readonly=False)
    resp, items = imap.search(None, 'All')
    email_ids = items[0].split()
    return imap, email_ids


def read_attachments(msg):
    """
    Reads through the attachments of an email and returns the contents in a dictionary
    :param msg: Type: Message object, the email to check the content of
    :return: dictionary of txt files in the email where the file's name is the key and
    an uppercase version of the content is the value
    """
    att_dict = {}
    allowed_exts = ".txt", ".text"
    for part_msg in msg.walk():
        if part_msg.get_content_maintype() == 'multipart':
            continue
        if part_msg.get('Content-Disposition') is None:
            continue
        filename = part_msg.get_filename()
        if filename is not None and filename.endswith(allowed_exts):
            file_content = part_msg.get_payload(decode=True).decode("utf-8").strip(" ")
            att_dict[filename] = file_content.upper()  # Uppercase since we check to an uppercase Subscription text
            logging.info(f"Found attachment named {filename} with content beginning with: "
                         f"{file_content[0:MSG_PRINT_LEN]}")
    return att_dict


def subscribe_user(addr_new, email_array, sub_unsub, time_data: TimeTrig, sender_info: Sender):
    """
    Either subscribes or unsubscribes a target. Function is called when a message in the emails is asking to either
    subscribe or unsubscribe
    :param addr_new: The email address the sub or unsub request came from
    :param email_array: = The data from the email yaml. Used because it'll need to update the subscribe flags in it
    :param sub_unsub: "Subscribe" = The request is to subscribe; "Unsubscribe" = The request is to unsubscribe
    :param time_data: Used when calling the send_emails function
    :param sender_info: Used when calling the send_emails function
    :return: Updated version of email_array
    """
    not_found_in_array = True
    addr_new = swap_mobile_sms_to_mms(addr_new)
    target_data = email_array['Target']
    if target_data is not None:  # If the string of emails is empty
        for target in target_data:
            if addr_new == target['Email']:
                subscribed_check = target['Subscribed']
                if subscribed_check:  # If subbed in yaml
                    if sub_unsub == "Unsubscribe":
                        logging.info(f"{addr_new} has unsubbed")
                        target['Subscribed'] = False
                    else:
                        logging.info(addr_new + " tried to sub, but already is")
                        return email_array  # Already subscribed
                else:
                    if sub_unsub == "Subscribe":
                        logging.info(f"{addr_new} has resubbed")
                        target['Subscribed'] = True
                    else:
                        logging.info(f"{addr_new} tried to unsub, but already is")
                        return email_array  # Already unsubbed
                not_found_in_array = False
                break
                # Address is already in the list
    if not_found_in_array:
        if sub_unsub == "Unsubscribe":  # email not in yaml and asking to unsub
            logging.info(f"{addr_new} tried to unsub, but isn't in the list")
            return email_array
        else:
            logging.info(f"{addr_new} has been added to the list")
            email_of_rcvr = addr_new
            name_of_rcvr = get_name(addr_new)
            fact_next_rcvr = 1
            subscribed_rcvr = True
            trivia_type_rcvr = CATEGORY_DEFAULT
            new_target = {'Email': email_of_rcvr, 'Name': name_of_rcvr, 'Fact_Next': fact_next_rcvr,
                          'Subscribed': subscribed_rcvr, 'Trivia_type': trivia_type_rcvr}
            if target_data is None:
                target_data = [new_target]
            else:
                target_data.append(new_target)
    email_array['Target'] = target_data
    user_to_send = [next((item for item in target_data if item["Email"] == addr_new), None)]
    send_emails(user_to_send, time_data, sender_info, sub_unsub=sub_unsub)
    return email_array


def move_to_label(imap, mailid, dest):
    """
     Moves an email into another label by copying it and then deleting it from the original location
    :param imap: an object that represents the emails in the inbox
    :param mailid: The number in the inbox of the mail to be moved
    :param dest: The label to move to
    :return: Nothing
    Code was heavily inspired by: https://newbedev.com/move-an-email-in-gmail-with-python-and-imaplib
    """
    global DISARM
    if DISARM:
        logging.info(f"Disarmed; email would have been moved to folder: {dest}")
        return
    # Sorts the data into folders on whether it was acted on
    # Gets the UID
    resp, data = imap.fetch(mailid, "(UID)")
    unique_id = data[0].decode("utf-8")
    unique_id = parse_uid(unique_id)
    # Copies from inbox to destination folder
    result = imap.uid('COPY', unique_id, dest)
    # Deletes the item in the inbox
    if result[0] == 'OK':
        logging.info(f"Email has been moved to folder: {dest}")
        imap.uid('STORE', unique_id, '+FLAGS', '(\Deleted)')
        imap.expunge()


def parse_inbox(sender_info: Sender, email_list, time_data: TimeTrig):
    """
    Combs through the inbox and checks if any of the messages have the subject or body of the sub or unsub keywords
    If so, then it will call the sub/unsub function
    :param sender_info: Email, password, and name being sent from
    :param email_list: Used by subscribe_user function
    :param time_data: Used by subscribe_user function
    :return: email_list and time_data pass-through for any updates the subscribe_user function creates
    """
    global SUB_TXT, UNSUB_TXT, DEST_BOX_ACTED, DEST_BOX_NOT_ACTED
    # Looks through the inbox for new mail
    # Checks if subscribe or unsubscribe
    # moves to correct inbox
    body_sub_txt = f">{SUB_TXT}<"
    body_unsub_txt = f">{UNSUB_TXT}<"
    chars_to_remove_msg = ["\r", "\n", " "]  # The characters to strip out of the message string
    """If email, then the html will cover the plaintext, but the html is wanted to verify 100% that only the data we're
    looking for is in an html section is the sub or unsub text 
    Checking if the string we want is alone in the HTML/MIME's message is likely not the ideal way to check, 
    but through testing on Yahoo mail, Outlook and Gmail, _every_ one of them encode differently and this was the 
    best compromise found after a few hours of research.
    Also, due to how Verizon iPhones send MMS, it now checks the text messages for txt files in them and 
    reads them as well.
    Possible edge-case: If the signature slot of the email has a stand-along section that says the sub or unsub text
    Issue: If someone using Outlook responds and has a signature, this won't catch it because Microsoft has the 
    signature and email exist in the same section. 
    The pandora's box of checking for any instance of the sub or unsub text in all email's plaintext isn't worth the 
    benefit of Outlook subscriptions working every time
    """
    imap, email_ids = check_for_new_mail(sender_info.email, sender_info.password)
    for em_id in email_ids:
        # Gets the Message
        dest = DEST_BOX_NOT_ACTED
        action = "Nothing"
        resp, data = imap.fetch(em_id, '(RFC822)')
        # if the data for the email is not found; Often happens when there are multiple emails to sort through,
        # it breaks on the last one. This is a bug, but is easy to ignore since the emails get checked every minute
        # Ex: 3 emails come in. The first two get executed, the last comes back without data
        # In 60 sec, that last email will be parsed through as well.
        if data[0] is None:
            break
        data_bytes = data[0][1]
        email_message = message_from_bytes(data_bytes)
        subject = email_message["subject"]
        addr_from = email_message["from"]
        if subject is None:
            subject = ""
        else:
            subject = subject.strip(" ")  # Gets rid of trailing spaces in the subject
        message = None
        for part in email_message.walk():
            if part.get_content_type() == "text/plain" or part.get_content_type() == "text/html":
                try:
                    message = part.get_payload(decode=True).decode('utf-8', errors='ignore')
                except UnicodeDecodeError as exception:
                    logging.warning(f"UnicodeDecodeError seen on message from: {addr_from}\n{exception}")
                    message = ""
        if message is None:
            message = ""
        logging.info(f"New email found with data: {repr(message[0:MSG_PRINT_LEN])}")  # Sends first 5 words
        # Checks if the email is a phone number. This is because phone number messages aren't HTML/MIME encoded,
        # while texts are
        is_text_msg = False if parse_email_for_phone(addr_from) is None else True
        if is_text_msg:  # If text message, then strip out the whitespace characters
            for char_remove in chars_to_remove_msg:
                message = message.replace(char_remove, "")
        # Check is the data is subscribed or not. Not that AT&T messages come in with many whitespaces and
        # look like HTML text, so they'll be compared to body_sub_txt/body_unsub_txt
        if subject.upper() == SUB_TXT or (is_text_msg and message.upper() == SUB_TXT) or \
                (body_sub_txt in message.upper()):
            action = "Subscribe"
        elif subject.upper() == UNSUB_TXT or (is_text_msg and message.upper() == UNSUB_TXT) or \
                (body_unsub_txt in message.upper()):
            action = "Unsubscribe"
        elif is_text_msg:  # If it's a text message that wasn't acted on, check its attachments, since Verizon
            # iPhones were shown to send the text as an attachment
            att = read_attachments(email_message)
            if UNSUB_TXT in att.values():
                action = "Unsubscribe"
            elif SUB_TXT in att.values():
                action = "Subscribe"
        # Moves the data to the correct inbox
        if action == "Subscribe":
            email_list = subscribe_user(addr_from, email_list, "Subscribe", time_data, sender_info)
            dest = DEST_BOX_ACTED
        elif action == "Unsubscribe":
            email_list = subscribe_user(addr_from, email_list, "Unsubscribe", time_data, sender_info)
            dest = DEST_BOX_ACTED
        elif action == "Nothing":
            pass
        else:
            logging.warning(f"When attempting to see if to sub or unsub, this action was seen: {action}")
        move_to_label(imap, em_id, dest)  # This line moves emails to a different label
    disconnect(imap)
    return email_list


def swap_mobile_sms_to_mms(email_phone):
    """
    Checks if an email is the SMS version of a provider's message, and converts it to MMS
    :param email_phone: The email being checked as a number
    :return: The phone number if it is one; None o/w
    """
    mobile_sms_to_mms = {
        "@txt.att.net": "@mms.att.net",
        "@vtext.com": "@vzwpix.com",
        "@messaging.sprintpcs.com": "@pm.sprint.com",
        "@vmobl.com": "@vmpix.com",
        "@sms.myboostmobile.com": "@myboostmobile.com",
        "@sms.cricketwireless.net": "@mms.cricketwireless.net",
        "@email.uscc.net": "@mms.uscc.net"}
    for key in mobile_sms_to_mms.keys():
        # replaces the SMS email with the MMS email; Lightly tested that SMS doesn't work
        if key in email_phone:
            email_phone = email_phone.replace(key, mobile_sms_to_mms[key])
            logging.info(f"Email found to be SMS number. Replaced with MMS: {email_phone}")
            break
    return email_phone


def get_name(email_addr):
    """
    Checks if an email is the SMS version of a provider's message, and converts it to MMS
    :param email_addr: The email being checked for a name (ex: 'John Smith <example@email.com>)
    :return: The name (ex of above: 'John Smith')
    """
    is_text_msg = False if parse_email_for_phone(email_addr) is None else True
    if is_text_msg:  # Don't return a name if it's a text message
        return ""
    name = ""
    found_start_email = False
    start_of_email = ["<"]
    # Pulls out the name from the sender data. Ignores it if it's a number of the email.
    addr_array = (email_addr.split(' '))
    for address in addr_array:
        if address[0] not in start_of_email:
            name += address + " "
        else:
            found_start_email = True
    if not found_start_email:
        return ""
    # The appending loop above will add an unnecessary space at the end. This code strips it
    if len(name) > 0 and name[-1] == " ":
        name = name[:-1]
    return name


def check_password_and_auth(sender_info: Sender):
    """
    Checks if the password in the yaml file is legit. If not, it'll ask for one to be typed in
    :param sender_info: Email, Name and Password of email being sent from
    :return: Nothing
    """
    pass_tested = False
    password = sender_info.password
    if password:  # If there is data in the password field, check it immediately
        pass_tested = send_emails("", "", sender_info, cred_test=True)  # test credentials
        if not pass_tested:
            logging.error("Authentication failed; Try changing the password")
    if not pass_tested:  # If the above if statement fails or password isn't filled in the code, it'll ask for it.
        while not pass_tested:
            password = input("Type your password and press enter: ")
            pass_tested = send_emails("", "", sender_info, cred_test=True)  # test credentials
            if not pass_tested:
                logging.error("Authentication failed; Try changing the password")
    logging.info("Email authentication passed")
    return password


def test():
    """
    Tests are limited as the Email.yml file will not live in the Git repo, so email lists and the authentication.
    Data to check emails isn't available
    :return: Nothing
    """
    global TRIVIA_ARRAY, HOUR_DEFAULT, MINUTE_DEFAULT
    yaml_max_print = 5
    print(f"Testing reading Trivia YAML")
    for fact_type in TRIVIA_ARRAY.keys():
        yaml_print = min(yaml_max_print, len(TRIVIA_ARRAY[fact_type]))
        print(f"\nPrinting last {str(yaml_print)} items in Fact Type: {fact_type}.")
        for i in range(-1 * yaml_print, 0):  # Messages are left
            print(TRIVIA_ARRAY[fact_type][i])
    print("\nTesting a sample email message:")
    minute_trig = random_min(MINUTE_DEFAULT)
    hour_trig = random_hour(HOUR_DEFAULT)
    date_trig = tz(TIMEZONE).localize(datetime.datetime.now())  # Get the current time
    time_trg_data = TimeTrig(hour_trig, minute_trig, date_trig)
    time_trg_data.last_wanted_trig = time_trg_data.next_trig
    # Comment the prev line to see the 'sorry for interruption' message since the code will think we triggered at a
    # time that's not our wanted trigger time
    sender = Sender("example@email.com", "example_password", "John Smith")
    msg, _ = create_message(rcvr_name="test receiver", trivia_index=2, sender_info=sender, time_data=time_trg_data,
                            target_email="receiver@email.com", sub_unsub="Unchanged", triv_type=CATEGORY_DEFAULT)
    msg = msg.replace('\n\n', '\n')  # Strips out multiple newlines for cleaner output
    msg = re.sub('\n\n+', '\n', msg)
    print(msg)


def main_loop():
    global TRIVIA_ARRAY, SEED_RAND, TRIG_CODE_AT_START, SEC_EMAIL_CHECK
    with open('Emails.yml', 'r', encoding='utf-8') as file_email:
        email_data = safe_load(file_email)
    email_data_prev = deepcopy(email_data)
    sender = Sender(email_data['Sender']['Email'], email_data['Sender']['Password'], email_data['Sender']['Name'])
    seed(SEED_RAND)
    hour_trig, minute_trig, date_trig = get_yml_time_data(email_data['Next_Send_Data'])
    sender.password = check_password_and_auth(sender)
    time_trg_data = TimeTrig(hour_trig, minute_trig, date_trig)
    if TRIG_CODE_AT_START:  # Sets the current time as the first trigger time
        time_trg_data.next_trig = tz(TIMEZONE).localize(datetime.datetime.now())
    logging.info(f"Next text to be sent at {time_trg_data.next_trig_str} on {time_trg_data.next_trig.date()}")
    while True:
        curr_time = tz(TIMEZONE).localize(datetime.datetime.now())  # Get the current time
        if curr_time.date() >= time_trg_data.next_trig.date() and curr_time.hour == time_trg_data.next_trig.hour and \
                curr_time.minute == time_trg_data.next_trig.minute:
            # Checks if day is on or later than the wanted in case there's a 24+hr outage;
            # it's preferred to send the message the next day rather than in a month
            minute_trig = random_min(minute_trig)
            hour_trig = random_hour(hour_trig)
            time_trg_data.create_next_trig(hour_trig, minute_trig, curr_time)
            time_trg_data.next_trig = modify_next_time_sent(time_trg_data.next_trig)
            email_data['Target'] = send_emails(email_data['Target'], time_trg_data, sender)
            # Dumps next time to send and next fact into the yaml, so it'll know where to go if the program restarts
            email_data['Next_Send_Data']['hr_next'] = time_trg_data.next_trig.hour
            email_data['Next_Send_Data']['min_next'] = time_trg_data.next_trig.minute
            email_data['Next_Send_Data']['date_next'] = time_trg_data.next_trig.date()
            logging.info(f"Next text to be sent at {time_trg_data.next_trig_str} on {time_trg_data.next_trig.date()}")
        else:
            # If next message is set at a time that now longer happens today
            if time_trg_data.next_trig.date() == curr_time.date():
                today_tmrw_txt = f"today at {time_trg_data.next_trig_str}"
            else:
                today_tmrw_txt = f"tomorrow at {time_trg_data.next_trig_str}"
            curr_time_str = curr_time.strftime("%I:%M:%S %p").lstrip("0")
            logging.debug(f"Not sent as time is: {curr_time_str}; waiting for {today_tmrw_txt}")
            # If we're to trigger in the next minute, this will reset the clock so that the emails will be sent at
            # the start of the minute and avoid parsing the inbox, which could cause the time to trigger to get missed
            # Ex: if the code wasn't here, and it's 5:00.59, and the code will trigger at 5,
            # then it'll send at 5:01 while the email calls cause delays
            time_min_prev_datetime = time_trg_data.next_trig - datetime.timedelta(minutes=1)
            if curr_time.date() >= time_min_prev_datetime.date() and curr_time.hour == time_min_prev_datetime.hour \
                    and curr_time.minute == time_min_prev_datetime.minute:
                logging.info(f"Waiting for the start of the minute; Current second is: "
                             f"{str(tz(TIMEZONE).localize(datetime.datetime.now()).second)}")
                while tz(TIMEZONE).localize(datetime.datetime.now()).minute != time_trg_data.next_trig.minute:
                    # What until the beginning of a min to start the loop to avoid the incorrect time due to delays
                    # in sending the message
                    pass
            else:
                email_data = parse_inbox(sender, email_data, time_trg_data)
                if email_data != email_data_prev:  # Updates the yaml if data changed
                    email_data_prev = deepcopy(email_data)
                    with open('Emails.yml', 'w') as file_email:
                        dump(email_data, file_email, default_flow_style=False, sort_keys=False)
                sleep(SEC_EMAIL_CHECK)
                # Unneeded, but takes up less processing since the code only needs to be checked once a minute;
                # Also stops infinite scrapping of Gmail


if __name__ == "__main__":
    with open('Facts.yml', 'r', encoding='utf-8') as file_trivia:
        TRIVIA_ARRAY = safe_load(file_trivia)  # Only place where TRIVIA_ARRAY is to be written
    CATEGORY_DEFAULT = list(TRIVIA_ARRAY.keys())[0]  # Makes this the first key found in the array
    if len(argv) > 1 and argv[1] in ["--test", "--t"]:
        test()
    else:
        main_loop()
