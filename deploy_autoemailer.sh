#!/bin/bash

#This code will get the newest commit of the Auto_Emailer.py code in the current branch, and deploys it

git -C ~/GIT/auto_emailer/ branch

git -C ~/GIT/auto_emailer/ pull

sudo rm /opt/Cat_Facts/Auto_Emailer.py

sudo cp ~/GIT/auto_emailer/Auto_Emailer.py /opt/Cat_Facts/.

sudo chown catfacts:catfacts /opt/Cat_Facts/Auto_Emailer.py -v

ls -la /opt/Cat_Facts/

sudo systemctl restart catfacts.service

sudo journalctl -ru catfacts.service
